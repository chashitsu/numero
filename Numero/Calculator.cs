﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Numero
{
   public static class Calculator
    {

        public static int IntToSingleNumInt(int rTemp)
        {
            if (rTemp > 99)
            {
                throw new ArgumentOutOfRangeException("tahled metoda je pouzitelna jen na inteeger 0 - 99 (dva znaky)");
            }
            var result = rTemp > 9 ? int.Parse(rTemp.ToString()[0].ToString()) + int.Parse(rTemp.ToString()[1].ToString()) : rTemp;
            return result;
        }
       

        public static int IntToSingleNumIntUltimate(string input)
        {
            
            if (input == "11" || input.Length < 2)
            {
                return int.Parse(input);
            }
            else
            {
                return IntToSingleNumIntUltimate( input.ToCharArray().Select(x => long.Parse(x.ToString())).Sum().ToString() );
            }
        }

        public static int CountStringIntByInt(string input)
        {
            var result = 0;
            if (string.IsNullOrEmpty(input))
            {
                return result;
            }

            if (input == "11")
            {
                return int.Parse(input);
            }
            foreach (var item in input)
            {
                result += int.Parse(""+item);
            }

            return result;
        }


            public static int Count4charsString(string input)
        {
            var result = 0;
            if (string.IsNullOrEmpty(input) || input.Length != 4)
            {
                //throw new Exception("pole ROK musi obsahovat presne 4 cisla");
            }
            else
            {
                try
                {
                    result = int.Parse(input[0].ToString()) + int.Parse(input[1].ToString()) + int.Parse(input[2].ToString()) + int.Parse(input[3].ToString());

                }
                catch (Exception)
                {
                    throw new Exception("pole ROK musi obsahovat presne 4 cislice");
                }
            }
            return result;
        }
    }
}
