﻿using System;
using System.Windows;

namespace Numero
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructors

        // spusteni s otevrenim souboru
        public MainWindow(string fileToOpen)
        {
            this.Initialized += (s, e) => 
            {
                var model = new MainWindowModel();
                this.DataContext = model;
                model.OpenFromFile(fileToOpen);
            };
            InitializeComponent();

        }

     

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowModel();
        }

        #endregion Constructors

        #region Methods

        private void Button_run(object sender, RoutedEventArgs e)
        {
            var model = this.DataContext as MainWindowModel;

            if (model != null)
            {
                var runResult = model.Run();
                if (runResult != "0")
                {                
                    var errWin = new ErrWindow();
                    errWin.Show();
                }
            }
        }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            var model = this.DataContext as MainWindowModel;
            if (model != null)
            {
                model.SaveToNumeroFile();
                //model.SaveToHtml();
            }
        }

        private void fillTheMatrix()
        {
            var date = this.day.Text + this.month.Text + this.year.Text;

            var m1Value = string.Empty;
            var m2Value = string.Empty;
            var m3Value = string.Empty;
            var m4Value = string.Empty;
            var m5Value = string.Empty;
            var m6Value = string.Empty;
            var m7Value = string.Empty;
            var m8Value = string.Empty;
            var m9Value = string.Empty;

            foreach (var dateCh in date)
            {
                switch (dateCh)
                {
                    case '1':
                        m1Value += "1";
                        break;

                    case '2':
                        m2Value += "2";
                        break;

                    case '3':
                        m3Value += "3";
                        break;

                    case '4':
                        m4Value += "4";
                        break;

                    case '5':
                        m5Value += "5";
                        break;

                    case '6':
                        m6Value += "6";
                        break;

                    case '7':
                        m7Value += "7";
                        break;

                    case '8':
                        m8Value += "8";
                        break;

                    case '9':
                        m9Value += "9";
                        break;

                    default:
                        break;
                }
            }

            this.m1.Text = m1Value;
            this.m2.Text = m2Value;
            this.m3.Text = m3Value;
            this.m4.Text = m4Value;
            this.m5.Text = m5Value;
            this.m6.Text = m6Value;
            this.m7.Text = m7Value;
            this.m8.Text = m8Value;
            this.m9.Text = m9Value;
        }

        private int intToSingleNumInt(int rTemp)
        {
            var result = rTemp > 9 ? int.Parse(rTemp.ToString()[0].ToString()) + int.Parse(rTemp.ToString()[1].ToString()) : rTemp;
            return result;
        }

        private void MainWindow_Initialized(object sender, EventArgs e)
        {
            // pridani Main modelu
            this.DataContext = new MainWindowModel();
        }

        /*
         - Vrcholy piramidy uz se nescitaji?
         - Jsou nejake pripady kdy by se cislo na vrcholu melo secist?

         - podbarvovani prazdnych ramecku mrizky?
         - obarveni nejakych plnych / prazdnych sloupcu?

           */

        private void print_Click(object sender, RoutedEventArgs e)
        {
            var model = this.DataContext as MainWindowModel;
            if (model != null)
            {
                model.Print();
            }
        }

        #endregion Methods

        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            var model = this.DataContext as MainWindowModel;
            if (model != null)
            {
                model.Open();
            }


        }
    }
}