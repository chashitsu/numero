﻿using System;

using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Xml;
using Numero.UserControls;

namespace Numero
{
    public class MainWindowModel : INotifyPropertyChanged, INameValueModelParent
    {
        #region Fields

        private string day;

        private string firstName;

        private string lastName;

        private string month;

        private string notes;

        private string year;

        #endregion Fields
        MatrixUcModel matrixFirstNameModel;

        [Bindable(true)]
        public MatrixUcModel MatrixFirstNameModel { get => this.matrixFirstNameModel; }
        MatrixUcModel matrixLastNameModel;

        [Bindable(true)]
        public MatrixUcModel MatrixLastNameModel { get => this.matrixLastNameModel;  }

        MatrixUcModel matrixThirdNameModel;

        [Bindable(true)]
        public  MatrixUcModel MatrixThirdNameModel { get => this.matrixThirdNameModel; }

        #region Constructors


        public Visibility DebugVisibility
        {
            get;
            set;
        }
        public MainWindowModel()
        {
            //if (System.Configuration.ConfigurationManager.AppSettings["DeveloperMode"] == "1")
            //{
            //    this.DebugVisibility = Visibility.Visible;
            //    OnPropertyChanged("DebugVisibility");
            //}

            this.Triangle = new TriangleModel();
            this.Matrix = new MatrixModel();
            this.NameValueModel = new NameValueModel(this);

            this.matrixFirstNameModel = new MatrixUcModel();
            this.matrixLastNameModel = new MatrixUcModel();
            this.matrixThirdNameModel = new MatrixUcModel();

            var notes = "Nepodařilo se načíst text";

            if (File.Exists("Notes.txt"))
            {
                notes = File.ReadAllText("Notes.txt");
            }
            this.Notes = notes;

            //   this.OnPropertyChanged("FirstName");
            //   this.OnPropertyChanged("LastName");
        }

        #endregion Constructors

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        // prvni cislice dne (int)
        public int D1
        {
            get
            {
                var result = 0;

                if (this.Day != null)
                {
                    int.TryParse(this.Day[0].ToString(), out result);
                }
                return result;
            }
        }

        internal void SaveToNumeroFile()
        {

            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                //dataSvg.Save(saveFileDialog.FileName + ".html");
                var result = string.Empty;

                // bude potreba ulozit
                // jmeno, prijmeni, tretiJmeno
                // datum narozeni
                // poznamky
                result = string.Format("{0};{1};{2};{3};{4};{5};\n{6}",
                                        this.FirstName, // 0 
                                        this.LastName,  // 1
                                        this.ThirdName, // 2
                                        this.Day,       // 3
                                        this.Month,     // 4
                                        this.Year,      // 5
                                        this.Notes);    // 6

                var fileName = saveFileDialog.FileName;
                if (!fileName.Contains(".numero"))
                {
                    fileName = fileName + ".numero";
                }

                File.WriteAllText(fileName, result);

                // DUPLICITNI UKLADANI HTML
                // pro zachovani puvodnich hodnot (vzazenych k puvodnimu datu), ulozim i HTML, kde jsou hodnoty napevno
                var htmlDoc = this.GetHtmlWithData();
                var htmlFileName = string.Format("{0}_{1:dd-MM-yyyy}.html", fileName.Remove(fileName.Length - 7), DateTime.Now);
                if (File.Exists(htmlFileName))
                {
                    File.Delete(htmlFileName);
                }

                htmlDoc.Save(htmlFileName);
            }

            

        }

        // druha cislice dne (int)
        public int D2
        {
            get
            {
                var result = 0;

                if (this.Day != null && this.Day.Length == 2)
                {
                    int.TryParse(this.Day[1].ToString(), out result);
                }
                return result;
            }
        }

        public string DateString
        {
            get { return string.Format("{0}.{1}.{2}", this.Day, this.Month, this.Year); }
        }

        public string Day { get => day; set => day = value; }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                this.firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get => lastName;
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }
        private string thirdName = string.Empty;
        public string ThirdName
        {
            get => thirdName;
            set
            {
                thirdName = value;
                OnPropertyChanged("ThirdName");
            }
        }

        // prvni cislice mesice (int)
        public int M1
        {
            get
            {
                var result = 0;

                if (this.Day != null)
                {
                    int.TryParse(this.Month[0].ToString(), out result);
                }
                return result;
            }
        }

        internal void Open()
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Numero (*.numero)|*.numero";
            if (openFileDialog.ShowDialog() == true)
            {
                OpenFromFile(openFileDialog.FileName);
            }

        }

        public void OpenFromFile(string fileName)
        {
            var inputFile = File.ReadAllText(fileName);
            var inputRows = inputFile.Split('\n');
            var firstRow = inputRows[0];

            var text = string.Empty;
            var i = 0;
            foreach (var row in inputRows)
            {
                if (i != 0)
                {
                    text = text + row + "\n";
                }
                i++;
            }


            this.Notes = text;
            this.FirstName = firstRow.Split(';')[0];
            this.LastName = firstRow.Split(';')[1];
            this.ThirdName = firstRow.Split(';')[2];
            // Tyto property nemaji implementovane strileni propertyChanged
            this.Day = firstRow.Split(';')[3];
            this.OnPropertyChanged("Day");
            this.Month = firstRow.Split(';')[4];
            this.OnPropertyChanged("Month");
            this.Year = firstRow.Split(';')[5];
            this.OnPropertyChanged("Year");

            this.Run();
        }

        // druha cislice mesice (int)
        public int M2
        {
            get
            {
                var result = 0;

                if (this.Day != null && this.Month.Length == 2)
                {
                    int.TryParse(this.Month[1].ToString(), out result);
                }

                return result;
            }
        }

        public MatrixModel Matrix { get; private set; }

        public string Month { get => month; set => month = value; }

        public NameValueModel NameValueModel { get; private set; }

        public string Notes { get => notes; set => notes = value; }

        public string OrvLabel
        {
            get
            {
                var result = string.Empty;
                if (IsDataInputReady())
                {
                    result = string.Format("ORV: {0} + {1} + {2} = {3}", this.Day, this.Month, getYearForOrv(), GetOrv());
                }
                return result; // ORV (top) label
            }
        }

        private bool IsDataInputReady()
        {
            return this.Day != null && this.Month != null && this.Year != null;
        }

        public int R
        {
            get
            {
                return Calculator.IntToSingleNumIntUltimate(this.Year);

                //// tohle pracuje spatne! pro rok 1963 ma vratit 1 namisto 10
                //// rok int
                //var rTemp = Calculator.Count4charsString(this.Year);
                //// rok jednomistny int
                //var rFinal = intToSingleNumInt(rTemp);
                //return rFinal;
            }
        }

        // popisek trojuhelniku
        // cislo aktualniho cyklu / ORV
        public string TLabel
        {
            get
            {
                var result = string.Empty;
                if (this.IsDataInputReady())                
                {

                    var firstCycleNumber = getFCNumber();
                    var ageYears = getVekObj().PocetRokov;

                    var orv = GetOrvIntValue();

                    // vypocet zacatku jednotlivych cyklu
                    //var c0 = firstCycleNumber - 1;
                    var c1 = firstCycleNumber;
                    var c2 = c1 + 9;
                    var c3 = c2 + 9;
                    var c4 = c3 + 9;


                    if (ageYears >= c4)
                    {
                        result = string.Format("{0} / {1}", this.Triangle.T1, orv);
                    }
                    else if (ageYears >= c3)
                    {
                        result = string.Format("{0} / {1}", this.Triangle.T2, orv);
                    }
                    else if (ageYears >= c2)
                    {
                        result = string.Format("{0} / {1}", this.Triangle.T4, orv);
                    }
                    else if (ageYears >= c1)
                    {
                        result = string.Format("{0} / {1}", this.Triangle.T3, orv);
                    }
                    else // nulty cyklus
                    { 
                        // POZOR nulty cyklus se jeste rozapda na tretiny a podle toho se vezme jedno ze tri spodnich cisel pod trojuhelnikem
                        var third = c1 / 3;

                        if (ageYears <= third)
                        {
                            result = string.Format("{0} / {1}", this.Triangle.T5, orv);
                        }
                        else if (ageYears <= third*2)
                        {
                            result = string.Format("{0} / {1}", this.Triangle.T6, orv);
                        }
                        else
                        {
                            result = string.Format("{0} / {1}", this.Triangle.T7, orv);
                        }

                    }

                }
                return result;
            }
        }

        // cislo prvniho cyklu, nebo tak neco...
        // vypocita se 36-ZČ
       
        private int getFCNumber()
        {
            return 36 - getZcInt();
        }
        

            public string FCNumberLabel
        {
            get {
                var result = string.Empty;
                if (!string.IsNullOrEmpty( this.Day))
                {
                    result = string.Format("První cyklus: {0}", getFCNumber());
                }
                return result;

            }
        }


        // Horniho label hned pod jmenem ( = )
        public string StarLabel
        {
            get
            {
                var topLabel = string.Empty;
                if (this.Day != null && this.Month != null && this.Year != null)
                {
                    topLabel = string.Format("ŽČ: {0} = {1}", this.DateString, this.getStarLabel());
                }
                return topLabel;
            }
        }

        public TriangleModel Triangle { get; private set; }

        public string Year { get => year; set => year = value; }

        #endregion Properties

        #region Methods

        //FCNumberLabel
        public int GetOrvIntValue()
        {
            var result = 0;

            
            int orvYear = getYearForOrv();
            var orvRaw = D1 + D2 + M1 + M2 + Calculator.Count4charsString(orvYear.ToString());
            var orvValue = orvRaw > 9 ? Calculator.IntToSingleNumInt(orvRaw) : orvRaw;
            if (orvValue > 9)
            {
                orvValue = Calculator.IntToSingleNumInt(orvValue);
            }
            result = orvValue;
            
            return result;
        }

        public string GetOrv()
        {
            var result = string.Empty;

            //// vyplneni ORV
            int orvYear = getYearForOrv();
            var orvRaw = D1 + D2 + M1 + M2 + Calculator.Count4charsString(orvYear.ToString());
            var orvValue = orvRaw > 9 ? Calculator.IntToSingleNumInt(orvRaw) : orvRaw;
            if (orvValue > 9)
            {
                orvValue = Calculator.IntToSingleNumInt(orvValue);
            }

            result = string.Format("{0} / {1}", orvRaw, orvValue);
            return result;
        }

        public void printImageUrlWithChrome(string url)
        {
            using (var process = new Process())
            {
                // Start Chrome in kiosk with kiosk printing (no printing confirmation window)
                process.StartInfo.FileName = @"c:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                process.StartInfo.Arguments = url;// +" --kiosk --kiosk-printing";

                process.Start();

                // Wait for Kiosk to load
                int millisecondWaitForChrome = 2000;
                System.Threading.Thread.Sleep(millisecondWaitForChrome);

                // Send CTRL + p
                System.Windows.Forms.SendKeys.SendWait("^(p)");
            }
        }

        Visibility btnPrintVisibility = Visibility.Hidden;
        Visibility btnSaveVisibility = Visibility.Hidden;
       public Visibility BtnPrintVisibility
        {
            get
            {
                return this.btnPrintVisibility;
            }
            private set
            {
                this.btnPrintVisibility = value;
                this.OnPropertyChanged("BtnPrintVisibility");

            }
        }
        public Visibility BtnSaveVisibility
        {
            get {
                return this.btnSaveVisibility;
            }
            private set {
                this.btnSaveVisibility = value;
                this.OnPropertyChanged("BtnSaveVisibility");                    
            } 
        }


        
        
        public string Run()
        {
            var result = Run_Logic();

            if (result == "0")
            {
                this.BtnPrintVisibility = Visibility.Visible;
                this.BtnSaveVisibility = Visibility.Visible;
            }
            else
            {
                this.BtnPrintVisibility = Visibility.Collapsed;
                this.BtnSaveVisibility = Visibility.Collapsed;
            }

            return result;
        }

        private string Run_Logic()
        {
            var d = 0;
            var m = 0;
            var y = 0;
            if (string.IsNullOrEmpty(this.Day) || string.IsNullOrEmpty(this.Month) || string.IsNullOrEmpty(this.Year) || !int.TryParse(this.Day, out d) || !int.TryParse(this.Month, out m) || !int.TryParse(this.Year, out y) || d > 31 || m > 12 || y < 0 || y > 9999 || string.IsNullOrEmpty(this.FirstName) || string.IsNullOrEmpty(this.LastName))
            {
                return "1";
            }

            // spocitani a refresh TROJUHELNIKu
            this.Triangle.FillThePyramid(this.D1, this.D2, this.M1, this.M2, this.R, GetOrv());
            // spocitani a refresh MATRIXu
            var star = Calculator.IntToSingleNumIntUltimate(getStarNumber()).ToString();
            this.Matrix.FillTheMatrix(this.Day, this.Month, this.Year, getStarLabel(), star);

            // spocitani hodnoty jmena
            this.NameValueModel.AllValuesPropertyChanged();

            // horni labely ORV a Star label
            this.OnPropertyChanged("AgeLabel");
            this.OnPropertyChanged("StarLabel");
            this.OnPropertyChanged("OrvLabel");
            this.OnPropertyChanged("FCNumberLabel");
            this.OnPropertyChanged("Notes"); // pokud doslo k nacteni ze souboru, je treba notifikovat zmenu do GUI
            this.OnPropertyChanged("TLabel");


            this.MatrixFirstNameModel.FillTheMatrix(GetNameValueString(this.FirstName), this.FirstName);

            this.MatrixLastNameModel.FillTheMatrix(GetNameValueString(this.LastName), this.LastName);
            if (!string.IsNullOrEmpty(this.ThirdName))
            {
                this.MatrixThirdNameModel.FillTheMatrix(GetNameValueString(this.ThirdName), this.ThirdName);
            }
            else
            {
                this.MatrixThirdNameModel.FillTheMatrix(string.Empty, string.Empty);
            }

            return "0";
        }

        private string GetNameValueString(string name)
        {
            return NameValueCalculator.GetNameValuesString(NameValueCalculator.RemoveDiacritism(name).ToUpper());
        }

        internal void Print()
        {
            XmlDocument doc = GetHtmlWithData();

            var outputPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "temp.html");
            if (File.Exists(outputPath))
            {
                File.Delete(outputPath);
            }

            doc.Save(outputPath);

            PrintFile(outputPath);
        }

        private VekObj getVekObj()
        {
            VekObj vek = new VekObj();
            vek.Calculate(new DateTime(int.Parse(this.Year), int.Parse(this.Month), int.Parse(this.Day)), DateTime.Now);
            return vek;
        }

        public string AgeLabel
        {
            get
            {
                var result = string.Empty;
                if (IsDataInputReady())
                {
                    var vek = getVekObj();             
                    result = string.Format("Věk: {0}  ({1:d.M.yyyy})", vek, DateTime.Now);
                }
                return result;
            }
        }

        internal void SaveToHtml()
        {
            var dataSvg = GetHtmlWithData();

            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                dataSvg.Save(saveFileDialog.FileName + ".html");
                //File.WriteAllText(, txtEditor.Text);
                //saveSvgAsPngImage(dataSvg, saveFileDialog.FileName + ".png");
                //saveSvg(dataSvg, saveFileDialog.FileName + ".svg");
            }
        }

        private static void saveSvg(XmlDocument doc, string outputPath)
        {
            doc.Save(outputPath);
        }

        //private static void saveSvgAsPngImage(XmlDocument doc, string outputPath)
        //{
        //    var svgDocument = SvgDocument.Open(doc);
        //    var bitmap = svgDocument.Draw();
        //    //save converted svg to file system
        //    var param = new EncoderParameters();

        //    bitmap.Save(outputPath, ImageFormat.Png);
        //}

        private void addInnerText(XmlElement element, XmlNode id, XmlDocument doc)
        {

            var txt = string.Empty;
            switch (id.Value)
            {
                case "AgeLabel":
                    element.InnerText = this.AgeLabel;
                    break;

                case "date1":
                    element.InnerText = string.Format("{0} {1} {2}", this.FirstName, this.LastName, this.ThirdName);
                    break;

                case "StarLabel":
                    element.InnerText = this.StarLabel;
                    break;

                case "OrvLabel":
                    element.InnerText = this.OrvLabel;
                    break;

                case "FCNumberLabel":
                    element.InnerText = this.FCNumberLabel ?? string.Empty;
                    break;

                case "t1":
                    element.InnerText = this.Triangle.T1;
                    break;

                case "t2":
                    element.InnerText = this.Triangle.T2;
                    break;

                case "t3":
                    element.InnerText = this.Triangle.T3;
                    break;

                case "t4":
                    element.InnerText = this.Triangle.T4;
                    break;

                case "t5":
                    element.InnerText = this.Triangle.T5;
                    break;

                case "t6":
                    element.InnerText = this.Triangle.T6;
                    break;

                case "t7":
                    element.InnerText = this.Triangle.T7;
                    break;


                // Matrix narozeni
                // ==============
                case "m11":
                    element.InnerText = this.Matrix.M1.Length > 4 ? this.Matrix.M1.Substring(0, 4) : this.Matrix.M1;
                    break;

                case "m12":
                    element.InnerText = this.Matrix.M1.Length > 4 ? this.Matrix.M1.Substring(4) : string.Empty;
                    break;

                case "m21":
                    element.InnerText = this.Matrix.M2.Length > 4 ? this.Matrix.M2.Substring(0, 4) : this.Matrix.M2;
                    break;

                case "m22":
                    element.InnerText = this.Matrix.M2.Length > 4 ? this.Matrix.M2.Substring(4) : string.Empty;
                    break;

                case "m31":
                    element.InnerText = this.Matrix.M3.Length > 4 ? this.Matrix.M3.Substring(0, 4) : this.Matrix.M3;
                    break;

                case "m32":
                    element.InnerText = this.Matrix.M3.Length > 4 ? this.Matrix.M3.Substring(4) : string.Empty;
                    break;

                case "m41":
                    element.InnerText = this.Matrix.M4.Length > 4 ? this.Matrix.M4.Substring(0, 4) : this.Matrix.M4;
                    break;

                case "m42":
                    element.InnerText = this.Matrix.M4.Length > 4 ? this.Matrix.M4.Substring(4) : string.Empty;
                    break;

                case "m51":
                    element.InnerText = this.Matrix.M5.Length > 4 ? this.Matrix.M5.Substring(0, 4) : this.Matrix.M5;
                    break;

                case "m52":
                    element.InnerText = this.Matrix.M5.Length > 4 ? this.Matrix.M5.Substring(4) : string.Empty;
                    break;

                case "m61":
                    element.InnerText = this.Matrix.M6.Length > 4 ? this.Matrix.M6.Substring(0, 4) : this.Matrix.M6;
                    break;

                case "m62":
                    element.InnerText = this.Matrix.M6.Length > 4 ? this.Matrix.M6.Substring(4) : string.Empty;
                    break;

                case "m71":
                    element.InnerText = this.Matrix.M7.Length > 4 ? this.Matrix.M7.Substring(0, 4) : this.Matrix.M7;
                    break;

                case "m72":
                    element.InnerText = this.Matrix.M7.Length > 4 ? this.Matrix.M7.Substring(4) : string.Empty;
                    break;

                case "m81":
                    element.InnerText = this.Matrix.M8.Length > 4 ? this.Matrix.M8.Substring(0, 4) : this.Matrix.M8;
                    break;

                case "m82":
                    element.InnerText = this.Matrix.M8.Length > 4 ? this.Matrix.M8.Substring(4) : string.Empty;
                    break;

                case "m91":
                    element.InnerText = this.Matrix.M9.Length > 4 ? this.Matrix.M9.Substring(0, 4) : this.Matrix.M9;
                    break;

                case "m92":
                    element.InnerText = this.Matrix.M9.Length > 4 ? this.Matrix.M9.Substring(4) : string.Empty;
                    break;



                // Hvezdicky v matrixu
                case "ms1":
                    element.InnerText = this.Matrix.MS1;
                    break;

                case "ms2":
                    element.InnerText = this.Matrix.MS2;
                    break;

                case "ms3":
                    element.InnerText = this.Matrix.MS3;
                    break;

                case "ms4":
                    element.InnerText = this.Matrix.MS4;
                    break;

                case "ms5":
                    element.InnerText = this.Matrix.MS5;
                    break;

                case "ms6":
                    element.InnerText = this.Matrix.MS6;
                    break;

                case "ms7":
                    element.InnerText = this.Matrix.MS7;
                    break;

                case "ms8":
                    element.InnerText = this.Matrix.MS8;
                    break;

                case "ms9":
                    element.InnerText = this.Matrix.MS9;
                    break;

                case "mlabel":
                    element.InnerText = this.Matrix.MLabel;
                    break;

                case "tlabel":
                    element.InnerText = this.TLabel;
                    break;

                // rozbor jmena:
                case "thirdNameValue1":
                    element.InnerText = this.NameValueModel.TN1;
                    break;
                case "thirdNameValue2":
                    element.InnerText = this.NameValueModel.TN2;
                    break;
                case "thirdNameValue3":
                    element.InnerText = this.NameValueModel.TN3Raw + this.NameValueModel.TN3;
                    break;
                case "l1":
                    element.InnerText = this.NameValueModel.L1;
                    break;

                case "l2":
                    element.InnerText = this.NameValueModel.L2;
                    break;

                case "l3":
                    element.InnerText = this.NameValueModel.L3Raw + this.NameValueModel.L3;
                    break;

                case "f1":
                    element.InnerText = this.NameValueModel.F1;
                    break;

                case "f2":
                    element.InnerText = this.NameValueModel.F2;
                    break;

                case "f3":
                    element.InnerText = this.NameValueModel.F3Raw + this.NameValueModel.F3;
                    break;

                case "count":
                    element.InnerText = this.NameValueModel.Count;
                    break;
                
                case "vowels1":
                    element.InnerText = this.NameValueModel.Vowels1;
                    break;

                case "vowels2":
                    element.InnerText = this.NameValueModel.Vowels2;
                    break;

                case "consonants1":
                    element.InnerText = this.NameValueModel.Consonants1;
                    break;

                case "consonants2":
                    element.InnerText = this.NameValueModel.Consonants2;
                    break;

                case "notes":
                    // specialni pripad, je treba text formatovat
                    addNoteChilds(element, doc, this.Notes);                    
                    break;



                // Matrixs First Name
                // ==================
                case "fm11":
                    txt = this.MatrixFirstNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm12":
                    txt = this.MatrixFirstNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm21":
                    txt = this.MatrixFirstNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm22":
                    txt = this.MatrixFirstNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm31":
                    txt = this.MatrixFirstNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm32":
                    txt = this.MatrixFirstNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm41":
                    txt = this.MatrixFirstNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm42":
                    txt = this.MatrixFirstNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm51":
                    txt = this.MatrixFirstNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm52":
                    txt = this.MatrixFirstNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm61":
                    txt = this.MatrixFirstNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm62":
                    txt = this.MatrixFirstNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm71":
                    txt = this.MatrixFirstNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm72":
                    txt = this.MatrixFirstNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm81":
                    txt = this.MatrixFirstNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm82":
                    txt = this.MatrixFirstNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "fm91":
                    txt = this.MatrixFirstNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "fm92":
                    txt = this.MatrixFirstNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                // Matrix Last Name
                // =================
                case "lm11":
                    txt = this.MatrixLastNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm12":
                    txt = this.MatrixLastNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm21":
                    txt = this.MatrixLastNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm22":
                    txt = this.MatrixLastNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm31":
                    txt = this.MatrixLastNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm32":
                    txt = this.MatrixLastNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm41":
                    txt = this.MatrixLastNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm42":
                    txt = this.MatrixLastNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm51":
                    txt = this.MatrixLastNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm52":
                    txt = this.MatrixLastNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm61":
                    txt = this.MatrixLastNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm62":
                    txt = this.MatrixLastNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm71":
                    txt = this.MatrixLastNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm72":
                    txt = this.MatrixLastNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm81":
                    txt = this.MatrixLastNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm82":
                    txt = this.MatrixLastNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "lm91":
                    txt = this.MatrixLastNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "lm92":
                    txt = this.MatrixLastNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                // Matrix Third Name
                // =================
                case "tm11":
                    txt = this.MatrixThirdNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm12":
                    txt = this.MatrixThirdNameModel.M1;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm21":
                    txt = this.MatrixThirdNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm22":
                    txt = this.MatrixThirdNameModel.M2;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm31":
                    txt = this.MatrixThirdNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm32":
                    txt = this.MatrixThirdNameModel.M3;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm41":
                    txt = this.MatrixThirdNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm42":
                    txt = this.MatrixThirdNameModel.M4;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm51":
                    txt = this.MatrixThirdNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm52":
                    txt = this.MatrixThirdNameModel.M5;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm61":
                    txt = this.MatrixThirdNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm62":
                    txt = this.MatrixThirdNameModel.M6;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm71":
                    txt = this.MatrixThirdNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm72":
                    txt = this.MatrixThirdNameModel.M7;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm81":
                    txt = this.MatrixThirdNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm82":
                    txt = this.MatrixThirdNameModel.M8;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;


                case "tm91":
                    txt = this.MatrixThirdNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(0, 4) : txt;
                    break;
                case "tm92":
                    txt = this.MatrixThirdNameModel.M9;
                    element.InnerText = txt.Length > 4 ? txt.Substring(4) : string.Empty;
                    break;

                // FirstName, LastName, ThirdName Matrix labels

                case "fmlabel":
                    element.InnerText = this.FirstName;
                    break;

                case "lmlabel":
                    element.InnerText = this.LastName;
                    break;

                case "tmlabel":
                    element.InnerText = this.ThirdName ?? string.Empty;
                    break;

                default:
                    break;
            }
        }

        private void addNoteChilds(XmlNode node, XmlDocument doc, string notes)
        {
            // tady by se asi hodilo napsat nejaky ten komentar...
            if (node != null)
            {
                // prachazi se pres vsechny radky (oddelovac je enter char(10))
                foreach (var row in notes.Split((char)10))
                {
                    // pro kazdy radek se vytvori odstavec (tag <p>)
                    var elementRow = doc.CreateElement("p");

                    // jestlize radek obsahuje znak '–' (dlouha pomlcka)
                    if (row.Contains("–"))
                    {
                        // rozdeli se radek podle tohoto znaku
                        var rowSplited = row.Split('–');

                        // prvni cast bude Bold
                        var rowBold = rowSplited[0];

                        // tuto prvni cast pak vymazu z pole
                        rowSplited[0] = string.Empty;

                        // tady se ulozi vsechny casti krom te prvni a vrati se pomlcky
                        var rowClear = string.Join(" – ", rowSplited);

                        // pridani elementu pro bold
                        var elementBold = doc.CreateElement("b");
                        elementBold.InnerText = rowBold;

                        // pridani bold elementu jako chuild row
                        elementRow.AppendChild(elementBold);

                        // nasetovani element.InnerText zpusobuje prepsani jiz pridaneho childu, je zkousim tedy pridat dalsi child pro zbyvajici cisty text
                        var elementDiv = doc.CreateElement("span");
                        elementDiv.InnerText = rowClear;
                        elementRow.AppendChild(elementDiv);

                        //elementRow.InnerText = string.Format("<b>{0}</b>{1}",rowBold,rowClear);
                        elementRow.SetAttribute("style", "font-family:monospace;font-size:17px;");
                    }
                    else
                    {
                        // pokud neobsahoval radek zadnou dlouhou pomlcku, tak hotovo
                        elementRow.InnerText = row;
                        elementRow.SetAttribute("style", "font-family:monospace;font-size:17px;");
                    }

                    // pridani tohoto radku do dokumentu
                    node.AppendChild(elementRow);
                }
            }
        }

        private string getPreparedNotes(string notes)
        {
            var result = string.Empty;

            var strBuilder = new StringBuilder();
            foreach (var row in notes.Split((char)10))
            {
                strBuilder.AppendFormat("{0}<br/>", row);
            }
            result = strBuilder.ToString();
            return result;
        }

        public int getZcInt()
        {
            var starNumber = getStarNumber();
            var result = Calculator.IntToSingleNumIntUltimate(starNumber);
            return result;
        }
        private string getStarLabel()
        {
            var topEqual = D1 + D2 + M1 + M2 + Calculator.Count4charsString(this.Year);
            var dateString = string.Format("{0}. {1}. {2}", this.Day, this.Month, this.Year);

            var starNumber = getStarNumber();

            var topLabelTmp = topEqual > 9 ? topEqual.ToString() + " / " + starNumber : starNumber;

            if (starNumber!="11" && starNumber.Length > 1)
            {
                topLabelTmp = topLabelTmp + " / " + Calculator.IntToSingleNumIntUltimate(starNumber).ToString();
            }

            return topLabelTmp;
        }

        private string getStarNumber()
        {
            var topEqual = D1 + D2 + M1 + M2 + Calculator.Count4charsString(this.Year);
            var dateString = string.Format("{0}. {1}. {2}", this.Day, this.Month, this.Year);

            var starNumber = string.Empty;
            if (topEqual > 9)
            {
                starNumber = (int.Parse(topEqual.ToString()[0].ToString()) + int.Parse(topEqual.ToString()[1].ToString())).ToString();
            }
            else
            {
                starNumber = topEqual.ToString();
            }
            return starNumber;
        }

        /// <summary>
        /// Hlavni tiskova metoda, nacte SVG resource do XML a dosadi do property spravne hodnoty
        /// </summary>
        /// <returns></returns>
        private XmlDocument GetHtmlWithData()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var imageStream = assembly.GetManifestResourceStream("Numero.drawing.html");

            var doc = new XmlDocument();

            doc.Load(imageStream);

            doc.IterateThroughAllNodes(
                delegate (XmlNode node)
                {
                    // ...Do something with the node...

                    if ((node as XmlElement) != null)
                    {
                        var element = (node as XmlElement);
                        var id = element.Attributes.GetNamedItem("id");
                        if (id != null)
                        {
                            addInnerText(element, id, doc);
                        }
                    }
                });

            return doc;
        }

        private int getYearForOrv()
        {
            var thisBirthday = new DateTime(DateTime.Now.Year, int.Parse(this.Month), int.Parse(this.Day));
            var orvYear = DateTime.Now < thisBirthday ? DateTime.Now.Year - 1 : DateTime.Now.Year;
            return orvYear;
        }

        private int intToSingleNumInt(int rTemp)
        {
            var result = rTemp > 9 ? int.Parse(rTemp.ToString()[0].ToString()) + int.Parse(rTemp.ToString()[1].ToString()) : rTemp;
            return result;
        }

        private void OnPropertyChanged(string prop)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void PrintFile(string fileName)
        {
            printImageUrlWithChrome(fileName);

            //using (var wb = new System.Windows.Forms.WebBrowser())
            //{
            //    wb.Navigate(fileName);
            //    while (wb.ReadyState != System.Windows.Forms.WebBrowserReadyState.Complete)
            //    {
            //        System.Windows.Forms.Application.DoEvents();
            //    }

            //    wb.Print();
            //}
        }

        #endregion Methods
    }
}