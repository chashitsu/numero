﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Numero
{
   public class MatrixModel : INotifyPropertyChanged
    {
        private void onPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public string M1 { get; private set; }
        public string M2 { get; private set; }
        public string M3 { get; private set; }
        public string M4 { get; private set; }
        public string M5 { get; private set; }
        public string M6 { get; private set; }
        public string M7 { get; private set; }
        public string M8 { get; private set; }
        public string M9 { get; private set; }
        public string MLabel { get; private set; }



        public string MS1 { get; private set; }
        public string MS2 { get; private set; }
        public string MS3 { get; private set; }
        public string MS4 { get; private set; }
        public string MS5 { get; private set; }
        public string MS6 { get; private set; }
        public string MS7 { get; private set; }
        public string MS8 { get; private set; }
        public string MS9 { get; private set; }
        /// <summary>
        ///  Fill the Matrix with fresh new data based on input parameters
        /// </summary>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public void FillTheMatrix(string day, string month, string year, string label, string star)
        {
            this.MLabel = label;

            var date = day + month + year;

            this.M1 = string.Empty;
            this.M2 = string.Empty;
            this.M3 = string.Empty;
            this.M4 = string.Empty;
            this.M5 = string.Empty;
            this.M6 = string.Empty;
            this.M7 = string.Empty;
            this.M8 = string.Empty;
            this.M9 = string.Empty;

            var previousChar = '-';
            var zeros = new string[9].Select(x => string.Empty).ToArray();


            foreach (var dateCh in date)
            {
                if (dateCh == '0')
                {
                    
                    switch (previousChar)
                    {
                        case '1':
                            zeros[0] += "0";
                            break;
                        case '2':
                            zeros[1] += "0";
                            break;
                        case '3':
                            zeros[2] += "0";
                            break;
                        case '4':
                            zeros[3] += "0";
                            break;
                        case '5':
                            zeros[4] += "0";
                            break;
                        case '6':
                            zeros[5] += "0";
                            break;
                        case '7':
                            zeros[6] += "0";
                            break;
                        case '8':
                            zeros[7] += "0";
                            break;
                        case '9':
                            zeros[8] += "0";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    previousChar = dateCh;
                }

                switch (dateCh)
                {
                    case '1':
                        this.M1 += "1";
                        break;
                    case '2':
                        this.M2 += "2";
                        break;
                    case '3':
                        this.M3 += "3";
                        break;
                    case '4':
                        this.M4 += "4";
                        break;
                    case '5':
                        this.M5 += "5";
                        break;
                    case '6':
                        this.M6 += "6";
                        break;
                    case '7':
                        this.M7 += "7";
                        break;
                    case '8':
                        this.M8 += "8";
                        break;
                    case '9':
                        this.M9 += "9";
                        break;
                    default:
                        break;
                }
            }

            int i = 0; 
            foreach (var zero in zeros)
            {
                switch (i.ToString())
                {
                    case "0":
                        this.M1 += zero;
                        break;
                    case "1":
                        this.M2 += zero;
                        break;
                    case "2":
                        this.M3 += zero;
                        break;
                    case "3":
                        this.M4 += zero;
                        break;
                    case "4":
                        this.M5 += zero;
                        break;
                    case "5":
                        this.M6 += zero;
                        break;
                    case "6":
                        this.M7 += zero;
                        break;
                    case "7":
                        this.M8 += zero;
                        break;
                    case "8":
                        this.M9 += zero;
                        break;
                    default:
                        break;
                }
                i++;
            }


            setStar(star);
            refreshAll();
        }

        private void setStar(string star)
        {
            

            this.MS1 = string.Empty;
            this.MS2 = string.Empty;
            this.MS3 = string.Empty;
            this.MS4 = string.Empty;
            this.MS5 = string.Empty;
            this.MS6 = string.Empty;
            this.MS7 = string.Empty;
            this.MS8 = string.Empty;
            this.MS9 = string.Empty;

            switch (star)
            {

                case "1":
                    this.MS1 = "*";
                    break;
                case "2":
                    this.MS2 = "*";
                    break;
                case "3":
                    this.MS3 = "*";
                    break;
                case "4":
                    this.MS4 = "*";
                    break;
                case "5":
                    this.MS5 = "*";
                    break;
                case "6":
                    this.MS6 = "*";
                    break;
                case "7":
                    this.MS7 = "*";
                    break;
                case "8":
                    this.MS8 = "*";
                    break;
                case "9":
                    this.MS9 = "*";
                    break;
                default:
                    break;
            }

        }

        private void refreshAll()
        {
            onPropertyChanged("M1");
            onPropertyChanged("M2");
            onPropertyChanged("M3");
            onPropertyChanged("M4");
            onPropertyChanged("M5");
            onPropertyChanged("M6");
            onPropertyChanged("M7");
            onPropertyChanged("M8");
            onPropertyChanged("M9");
            onPropertyChanged("MLabel");


            // hvezdicky
            onPropertyChanged("MS1");
            onPropertyChanged("MS2");
            onPropertyChanged("MS3");
            onPropertyChanged("MS4");
            onPropertyChanged("MS5");
            onPropertyChanged("MS6");
            onPropertyChanged("MS7");
            onPropertyChanged("MS8");
            onPropertyChanged("MS9");
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
