﻿namespace Numero
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    class NameValueCalculator
    {
        #region Methods

        public static string RemoveDiacritism(string Text)
        {
            string stringFormD = Text.Normalize(System.Text.NormalizationForm.FormD);
            System.Text.StringBuilder retVal = new System.Text.StringBuilder();
            for (int index = 0; index < stringFormD.Length; index++)
            {
                if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stringFormD[index]) != System.Globalization.UnicodeCategory.NonSpacingMark)
                    retVal.Append(stringFormD[index]);
            }
            return retVal.ToString().Normalize(System.Text.NormalizationForm.FormC);
        }

        public static string GetNameValuesString(string name)
        {
            var result = string.Empty;

            var nameAr = name.ToCharArray();
            for(int i = 0; i < name.Length ; i++)
            {
                result = result + GetCharValue(name[i]).ToString();
            }

            return result;
        }

        private static char[] vowels = new char[] { 'A','E','I','O','U','Y'};

        public static string GetAllVowels(string name)
        {
            var result = string.Empty;

            foreach (var item in name)
            {
                if (vowels.Contains(item))
                {
                    result = result + item;
                }
            }

            return result;
        }

        public static string GetAllConsonants(string name)
        {
            var result = string.Empty;

            foreach (var item in name)
            {
                if (!vowels.Contains(item))
                {
                    result = result + item;
                }
            }

            return result;
        }


        public static int GetNameCount(string name)
        {
            var result = 0;
            
            for (int i = 0; i < name.Length ; i++)
            {
                result += GetCharValue(name[i]);
            }
            return result;

        }

        public static int GetCharValue(char ch)
        {
            int result = 0;

            switch (ch)
            {

                case 'A':
                case 'J':
                case 'S':
                    result = 1;
                    break;
                case 'B':
                case 'K':
                case 'T':
                    result = 2;
                    break;
                case 'C':
                case 'L':
                case 'U':
                    result = 3;
                    break;
                case 'D':
                case 'M':
                case 'V':
                    result = 4;
                    break;
                case 'E':
                case 'N':
                case 'W':
                    result = 5;
                    break;

                case 'F':
                case 'O':
                case 'X':
                    result = 6;
                    break;
                case 'G':
                case 'P':
                case 'Y':
                    result = 7;
                    break;

                case 'H':
                case 'Q':
                case 'Z':
                    result = 8;
                    break;

                case 'I':
                case 'R':
                    result = 9;
                    break;

                default:
                    result = 0;
                    break;
            }
            return result;
        }

        #endregion Methods
    }
}