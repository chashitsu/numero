﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Numero
{
    public interface INameValueModelParent
    {
        string FirstName { get; }
        string LastName { get; }
        string ThirdName { get; }
    }

   public class NameValueModel : INotifyPropertyChanged

    {
        INameValueModelParent parent;
        public NameValueModel(INameValueModelParent parent)
        {
            this.parent = parent;
        }

        private void OnPropertyChanged(string prop)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public string LastName
        {
            get => this.parent.LastName;            
        }

        public string ThirdName
        {
            get => this.parent.ThirdName;
        }

        public string FirstName
        {
            get => this.parent.FirstName;           
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private bool isFormReady { get => !string.IsNullOrEmpty(this.FirstName) && !string.IsNullOrEmpty(this.LastName); }


        public string F1
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    result = NameValueCalculator.RemoveDiacritism(string.Join(" ",this.FirstName.ToUpper().ToCharArray() ));
                }
                return result;
            }
        }

        /// <summary>
        /// ciselne hodnoty jednotlivych pismen
        /// </summary>
        public string F2
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName).ToUpper();
                    var nameValue = NameValueCalculator.GetNameValuesString(name);
                    result = string.Join(" ", nameValue.ToCharArray());
                }
                return result;
            }
        }

        /// <summary>
        /// ciselne hodnoty jednotlivych pismen
        /// </summary>
        public string L2
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.LastName).ToUpper();
                    var nameValue = NameValueCalculator.GetNameValuesString(name);
                    result = string.Join(" ", nameValue.ToCharArray());                    
                }
                return result;
            }
        }

        private string tn3Raw = string.Empty;
        public string TN3Raw
        {
            get
            {
                return this.tn3Raw;
            }
            private set
            {
                this.tn3Raw = value;
                this.OnPropertyChanged("TN3Raw");
            }
        }

        private string l3Raw = string.Empty;
        public string L3Raw
        {
            get
            {
                return this.l3Raw;
            }
            private set
            {
                this.l3Raw = value;
                this.OnPropertyChanged("L3Raw");
                
            }

        }


        private string t3 = string.Empty;
        public string TN3
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.ThirdName).ToUpper();
                    result = NameValueCalculator.GetNameCount(name).ToString();

                    if (result.Length > 1) // je treba hodit tento vysledek pred pomlcku a dopocitat jeste soucet
                    {
                        this.TN3Raw = result + " / ";
                        result = Calculator.IntToSingleNumIntUltimate(result).ToString();
                    }
                }
                return result;
            }

        }

        private string f3Raw = string.Empty;
        public string F3Raw
        {
            get
            {                
                return this.f3Raw;
            }
            private set
            {
                this.f3Raw = value;
                this.OnPropertyChanged("F3Raw");

            }

        }


        /// <summary>
        /// ciselne hodnoty jednotlivych pismen
        /// </summary>
        public string L3
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.LastName).ToUpper();                    
                    result = NameValueCalculator.GetNameCount(name).ToString();

                    if (result.Length > 1) // je treba hodit tento vysledek pred pomlcku a dopocitat jeste soucet
                    {
                        this.L3Raw = result + " / ";
                       result = Calculator.IntToSingleNumIntUltimate(result).ToString();
                    }
                }
                return result;
            }
        }

        public string Vowels1
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName+this.LastName).ToUpper();
                    var vowels = NameValueCalculator.GetAllVowels(name).ToString();
                    result = string.Join(" ", vowels.ToCharArray());
                }
                return result;

            }

        }

        public string Vowels2
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName + this.LastName).ToUpper();
                    var vowels = NameValueCalculator.GetAllVowels(name).ToString();
                    var vowelsVal = NameValueCalculator.GetNameValuesString(vowels);
                    var vowelsCount = Calculator.IntToSingleNumIntUltimate(vowelsVal);

                    

                    result = string.Format( "{0} = {1} / {2}",string.Join(" ", vowelsVal.ToCharArray()), Calculator.CountStringIntByInt(vowelsVal), vowelsCount);
                }
                return result;

            }

        }

        public string Consonants2
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName + this.LastName).ToUpper();
                    var cons = NameValueCalculator.GetAllConsonants(name).ToString();
                    var consVal = NameValueCalculator.GetNameValuesString(cons);
                    var consCount = Calculator.IntToSingleNumIntUltimate(consVal);

                    result = string.Format("{0} = {1} / {2}", string.Join(" ", consVal.ToCharArray()), Calculator.CountStringIntByInt(consVal), consCount);
                }
                return result;

            }

        }

        public string Consonants1
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName + this.LastName).ToUpper();
                    var consonants = NameValueCalculator.GetAllConsonants(name);
                    result = string.Join(" ", consonants.ToCharArray());
                }
                return result;

            }

        }

        /// <summary>
        /// ciselne hodnoty jednotlivych pismen
        /// </summary>
        public string F3
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.FirstName).ToUpper();
                    result = NameValueCalculator.GetNameCount(name).ToString();


                    if (result.Length > 1) // je treba hodit tento vysledek pred pomlcku a dopocitat jeste soucet
                    {
                        this.F3Raw = result + " / ";
                        result = Calculator.IntToSingleNumIntUltimate(result).ToString();
                    }


                }
                return result;
            }
        }


        public string Count
        {
            get
            {

                var result = string.Empty;
                if (isFormReady)
                {
                    var name1 = NameValueCalculator.RemoveDiacritism(this.FirstName).ToUpper();
                    var name2 = NameValueCalculator.RemoveDiacritism(this.LastName).ToUpper();

                    var count = int.Parse(this.F3) + int.Parse(this.L3); //NameValueCalculator.GetNameCount(name1 + name2);

                    if (count > 9 )
                    {
                        var final = Calculator.IntToSingleNumIntUltimate(count.ToString());
                        result = string.Format("{0} / {1}", count, final);
                    }
                    else
                    {
                        result = count.ToString();
                    }
                }
                return result;
            }
        }

        public string TN1
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    result = NameValueCalculator.RemoveDiacritism(string.Join(" ", this.ThirdName.ToUpper().ToCharArray()));
                }
                return result;
            }
        }

        public string TN2
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    var name = NameValueCalculator.RemoveDiacritism(this.ThirdName).ToUpper();
                    var nameValue = NameValueCalculator.GetNameValuesString(name);
                    result = string.Join(" ", nameValue.ToCharArray());
                }
                return result;
            }
        }

        public string L1
        {
            get
            {
                var result = string.Empty;
                if (isFormReady)
                {
                    result = NameValueCalculator.RemoveDiacritism(string.Join(" ", this.LastName.ToUpper().ToCharArray()));
                }
                return result;
            }
        }
        
        internal void AllValuesPropertyChanged()
        {
            OnPropertyChanged("F1");
            OnPropertyChanged("L1");
            OnPropertyChanged("F2");
            OnPropertyChanged("L2");
            OnPropertyChanged("F3");
            OnPropertyChanged("L3");
            OnPropertyChanged("F3Raw");
            OnPropertyChanged("L3Raw");
            OnPropertyChanged("TN1");
            OnPropertyChanged("TN2");
            OnPropertyChanged("TN3Raw");
            OnPropertyChanged("TN3");
            OnPropertyChanged("Count");
            OnPropertyChanged("Consonants1");
            OnPropertyChanged("Consonants2");
            OnPropertyChanged("Vowels1");
            OnPropertyChanged("Vowels2");
        }
    }
}
