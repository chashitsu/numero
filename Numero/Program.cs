﻿using System;
using System.Windows;
using System.IO;

namespace Numero
{

    using System;
    using System.Windows;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
            [STAThread]
            static void Main(string[] args)
            {
                

                Application app = new Application();

                if (args != null && args.Length == 1)
                {

                    app.Run(new Numero.MainWindow(args[0]));
                }
                else
                {
                    app.Run(new Numero.MainWindow());

                }


        }

        }
}
