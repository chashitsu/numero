﻿namespace Numero
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

  public interface ITriangleModelParent
    {

    }

    public class TriangleModel : INotifyPropertyChanged

    {
        #region Properties


        public string Date1
        {
            get;
            set;
        }

        public string Date2
        {
            get;
            set;
        }

        private int T1value;

       // public string TLabel { get; private set; }

        private int T2value;
        private int T3value;
        private int T4value;
        private int T5value;
        private int T6value;
        private int T7value;
        private ITriangleModelParent parent;

        /// <summary>
        /// /
        /// </summary>
        /// <param name="d1">den[0]</param>
        /// <param name="d2">den[1]</param>
        /// <param name="m1">mesic[0]</param>
        /// <param name="m2">mesic[1]</param>
        /// <param name="rFinal"></param>
        public void FillThePyramid(int d1, int d2, int m1, int m2, int rFinal, string tLabel)
        {

            // plneni spodniho radku pod pyramidou
            this.T5value = Calculator.IntToSingleNumInt(m1 + m2);
            this.T6value = Calculator.IntToSingleNumInt(d1 + d2);
            this.T7value = rFinal;


            // plneni druheho radku
            this.T3value = Calculator.IntToSingleNumInt(this.T5value + this.T6value);
            this.T4value = Calculator.IntToSingleNumInt(this.T6value + this.T7value);


            // plneni tretiho radku
            this.T2value = this.T3value + this.T4value; //intToSingleNumInt(t3Value + t4Value);
            if (this.T2value > 9 && this.T2value != 11)
            {
                this.T2value = Calculator.IntToSingleNumIntUltimate(T2value.ToString());
            }
            


                // plneni ctvrteho radku
                this.T1value = this.T5value + this.T7value;
            if (this.T1value > 9 && this.T1value != 11)
            {
                this.T1value = Calculator.IntToSingleNumIntUltimate(T1value.ToString());
            }
          

            isIntialized = true;

            this.Refresh();
        }
        private bool isIntialized = false;

        public TriangleModel()
        {
         
        }

        public string T1
        {
            get => isIntialized ? this.T1value.ToString() : string.Empty;
        }

        public string T2
        {
            get => isIntialized ? this.T2value.ToString() : string.Empty;
        }

        public string T3
        {
            get => isIntialized ? this.T3value.ToString() : string.Empty;
        }

        public string T4
        {
            get => isIntialized ? this.T4value.ToString() : string.Empty;
        }

        public string T5
        {
            get => isIntialized ? this.T5value.ToString() : string.Empty;
        }

        public string T6
        {
            get => isIntialized ? this.T6value.ToString() : string.Empty;
        }

        public string T7
        {
            get => isIntialized ? this.T7value.ToString() : string.Empty;
        }

        private void onPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public int T1value1 {
            get => T1value;
            set { T1value = value; onPropertyChanged("T1"); onPropertyChanged("T1value"); }
        }
        public int T2value1 { get => T2value;
            set
            {                    
                T2value = value;                                                        
                onPropertyChanged("T2");
                onPropertyChanged("T2value");
            }
        }
        public int T3value1 { get => T3value;
            set { T3value = value; onPropertyChanged("T3"); onPropertyChanged("T3value"); }
        }
        public int T4value1 { get => T4value;
            set { T4value = value; onPropertyChanged("T4"); onPropertyChanged("T4value"); }
        }
        public int T5value1 { get => T5value;
            set { T5value = value; onPropertyChanged("T5"); onPropertyChanged("T5value"); }
        }
        public int T6value1 { get => T6value;
            set { T6value = value; onPropertyChanged("T6"); onPropertyChanged("T6value"); }
            }
        public int T7value1 { get => T7value;
            set { T7value = value; onPropertyChanged("T7"); onPropertyChanged("T7value"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Refresh()
        {
            onPropertyChanged("T1");
            onPropertyChanged("T2");
            onPropertyChanged("T3");
            onPropertyChanged("T4");
            onPropertyChanged("T5");
            onPropertyChanged("T6");
            onPropertyChanged("T7");
            onPropertyChanged("TLabel");
        }

        #endregion Properties

        #region Other


        #endregion Other
    }
}