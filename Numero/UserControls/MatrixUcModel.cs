﻿using System.ComponentModel;

namespace Numero.UserControls
{
    public class MatrixUcModel : INotifyPropertyChanged
    {
        public MatrixUcModel()
        {
        }

        private void onPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        [Bindable(true)]
        public string M1 { get; private set; }
        [Bindable(true)]
        public string M2 { get; private set; }
        [Bindable(true)]
        public string M3 { get; private set; }
        [Bindable(true)]
        public string M4 { get; private set; }
        [Bindable(true)]
        public string M5 { get; private set; }
        [Bindable(true)]
        public string M6 { get; private set; }
        [Bindable(true)]
        public string M7 { get; private set; }
        [Bindable(true)]
        public string M8 { get; private set; }
        [Bindable(true)]
        public string M9 { get; private set; }
        [Bindable(true)]
        public string MLabel { get; private set; }



        public void FillTheMatrix(string numbersString, string label)
        {
            this.MLabel = label;

            var date = numbersString;

            this.M1 = string.Empty;
            this.M2 = string.Empty;
            this.M3 = string.Empty;
            this.M4 = string.Empty;
            this.M5 = string.Empty;
            this.M6 = string.Empty;
            this.M7 = string.Empty;
            this.M8 = string.Empty;
            this.M9 = string.Empty;

            foreach (var dateCh in date)
            {
                switch (dateCh)
                {
                    case '1':
                        this.M1 += "1";
                        break;
                    case '2':
                        this.M2 += "2";
                        break;
                    case '3':
                        this.M3 += "3";
                        break;
                    case '4':
                        this.M4 += "4";
                        break;
                    case '5':
                        this.M5 += "5";
                        break;
                    case '6':
                        this.M6 += "6";
                        break;
                    case '7':
                        this.M7 += "7";
                        break;
                    case '8':
                        this.M8 += "8";
                        break;
                    case '9':
                        this.M9 += "9";
                        break;
                    default:
                        break;
                }
            }
            refreshAll();
        }

        private void refreshAll()
        {
            onPropertyChanged("M1");
            onPropertyChanged("M2");
            onPropertyChanged("M3");
            onPropertyChanged("M4");
            onPropertyChanged("M5");
            onPropertyChanged("M6");
            onPropertyChanged("M7");
            onPropertyChanged("M8");
            onPropertyChanged("M9");
            onPropertyChanged("MLabel");
        }

        public event PropertyChangedEventHandler PropertyChanged;



    }
}
