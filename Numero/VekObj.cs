﻿namespace Numero
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    // Prevzato od D+ a mirne doupraveno 20.3.2018 Lukas B
    /// <summary>
    /// Trieda pomocou metódy <see cref="Calculate"/> vie určiť vek osoby 
    /// Roky <see cref="PocetRokov"/>, Mesiace <see cref="PocetMesiacov"/> ň
    /// a Dni <see cref="PocetDni"/>.
    /// 
    /// </summary>
    public class VekObj : IComparable<VekObj>, IEquatable<VekObj>
    {
        #region Fields

        private int pocetDni = 0;
        private int pocetMesiacov = 0;
        private int pocetRokov = 0;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Bezparametrický konštruktor. Východnie hodntoy properties sú 0.
        /// </summary>
        public VekObj()
        {
        }

        /// <summary>
        /// Konštruktor s inicializáciou základných properties
        /// </summary>
        /// <param name="roky">Počet rokov od dátumu narodenia</param>
        /// <param name="mesiace">Počet mesiacov od dátumu narodenia</param>
        /// <param name="dni">Počet dní od dátumu narodenia</param>
        public VekObj(int roky, int mesiace, int dni)
            : this()
        {
            this.PocetRokov = roky;
            this.PocetMesiacov = mesiace;
            this.pocetDni = dni;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Get, Set Počet dní od dátumu narodenia
        /// </summary>
        public int PocetDni
        {
            get { return pocetDni; }
            set { pocetDni = value; }
        }

        /// <summary>
        /// Get <see cref="PocetDni"/> slovom.
        /// </summary>
        public string PocetDniText
        {
            get
            {
                switch (this.PocetDni)
                {
                    case 1:
                        return this.PocetDni.ToString() + " " + "den";
                    case 2:
                    case 3:
                    case 4:
                        return this.PocetDni.ToString() + " " + "dny";
                    default:
                        return this.PocetDni.ToString() + " " + "dní";
                }
            }
        }

        /// <summary>
        /// Get,Set Počet mesiacov od dátumu narodenia
        /// </summary>
        public int PocetMesiacov
        {
            get { return pocetMesiacov; }
            set { pocetMesiacov = value; }
        }

        /// <summary>
        /// Get <see cref="PocetRokov"/> slovom
        /// </summary>
        public string PocetMesiacovText
        {
            get
            {
                switch (this.PocetMesiacov)
                {
                    case 1:
                        return this.PocetMesiacov.ToString() + " " + "měsíc";
                    case 2:
                    case 3:
                    case 4:
                        return this.PocetMesiacov.ToString() + " " + "měsíce";
                    default:
                        return this.PocetMesiacov.ToString() + " " + "měsíců";
                }
            }
        }

        /// <summary>
        /// Get,Set Počet rokov od dátumu narodenia
        /// </summary>
        public int PocetRokov
        {
            get { return pocetRokov; }
            set { pocetRokov = value; }
        }

        /// <summary>
        /// Get <see cref="PocetRokov"/> slovom.
        /// </summary>
        public string PocetRokovText
        {
            get
            {
                switch (this.PocetRokov)
                {
                    case 1:
                        return this.PocetRokov.ToString() + " " + "rok";
                    case 2:
                    case 3:
                    case 4:
                        return this.PocetRokov.ToString() + " " + "roky";
                    default:
                        return this.PocetRokov.ToString() + " " + "roků";
                }
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Operátor porovná, či sa parametre líšia
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE = operátory nie sú zhodné</returns>
        public static bool operator !=(VekObj op1, VekObj op2)
        {
            return !(op1 == op2);
        }

        /// <summary>
        /// Operátor porovná, či <paramref name="op1"/> je menší ako <paramref name="op2"/>
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE: <paramref name="op1"/> je menší ako <paramref name="op2"/></returns>
        public static bool operator <(VekObj op1, VekObj op2)
        {
            if (((object)op1) == null)
                throw new ArgumentNullException("op1");
            else
                return op1.CompareTo(op2) < 0;
        }

        /// <summary>
        /// Operátor porovná, či <paramref name="op1"/> je menší alebo rovný ako <paramref name="op2"/>
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE: <paramref name="op1"/> je menší alebo rovný ako <paramref name="op2"/></returns>
        public static bool operator <=(VekObj op1, VekObj op2)
        {
            if (((object)op1) == null)
                throw new ArgumentNullException("op1");
            else
                return op1.CompareTo(op2) <= 0;
        }

        /// <summary>
        /// Operátor porovná, či sa parametre zhodujú
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE = operátory sú zhodné</returns>
        public static bool operator ==(VekObj op1, VekObj op2)
        {
            if (((object)op1) == null)
                return ((object)op2) == null;
            else
                return op1.Equals(op2);
        }

        /// <summary>
        /// Operátor porovná, či <paramref name="op1"/> je väčší ako <paramref name="op2"/>
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE: <paramref name="op1"/> je väčší ako <paramref name="op2"/></returns>
        public static bool operator >(VekObj op1, VekObj op2)
        {
            if (((object)op1) == null)
                throw new ArgumentNullException("op1");
            else
                return op1.CompareTo(op2) > 0;
        }

        /// <summary>
        /// Operátor porovná, či <paramref name="op1"/> je väčší alebo rovný ako <paramref name="op2"/>
        /// </summary>
        /// <param name="op1">prvý operand</param>
        /// <param name="op2">druhý operand</param>
        /// <returns>TRUE: <paramref name="op1"/> je väčší alebo rovný ako <paramref name="op2"/></returns>
        public static bool operator >=(VekObj op1, VekObj op2)
        {
            if (((object)op1) == null)
                throw new ArgumentNullException("op1");
            else
                return op1.CompareTo(op2) >= 0;
        }

        /// <summary>
        /// Funkcia na základe <paramref name="datumNarodenia"/>
        /// vypočíta hodnoty properties Počet rokov, počet dní a Počet mesiacov, dosiahnutých ku
        /// dňu. <paramref name="datum"/>
        /// </summary>
        /// <param name="datumNarodenie">dátum narodenia</param>
        /// <param name="datum">dátum dňa, ku ktorému sa zisťuje vek</param>
        public void Calculate(
            DateTime datumNarodenie,
            DateTime datum)
        {
            this.pocetDni = 0;
            this.pocetMesiacov = 0;
            this.pocetRokov = 0;
            if (datumNarodenie > datum)
                return;

            this.pocetDni = datum.Day - datumNarodenie.Day;

            if (pocetDni < 0)
            {
                //Inak počet zvyšných dní mesiaca narodenia + počet dní v aktuálnom mesiaci
                pocetDni =
                    DateTime.DaysInMonth(datumNarodenie.Year, datumNarodenie.Month) -
                    datumNarodenie.Day +
                    datum.Day;

                pocetMesiacov -= 1;

            }

            pocetMesiacov += datum.Month - datumNarodenie.Month;

            if (pocetMesiacov < 0)
            {

                pocetMesiacov += 12;
                pocetRokov -= 1;

            }

            pocetRokov += datum.Year - datumNarodenie.Year;
        }

        /// <summary>
        /// Metóda porovná, či this je menší rovný alebo vyšší ako <see cref="other"/>
        /// </summary>
        /// <param name="other">vek, s ktorým sa vykonáva porovnanie</param>
        /// <returns>
        /// -1..this je menčí ako <paramref name="other"/>
        ///  0..this je zhodný s <paramref name="other"/>
        ///  1..this je väčší ako <paramref name="other"/>
        /// </returns>
        public int CompareTo(VekObj other)
        {
            if (other == null)
                throw new ArgumentNullException("other");

            if (this.PocetRokov != other.PocetRokov)
                return this.PocetRokov.CompareTo(other.PocetRokov);
            else if (this.PocetMesiacov != other.PocetMesiacov)
                return this.PocetMesiacov.CompareTo(other.PocetMesiacov);
            else if (this.PocetDni != other.PocetDni)
                return this.PocetDni.CompareTo(other.PocetDni);
            else
                return 0;
        }

        public override bool Equals(object obj)
        {
            if (obj is VekObj)
                return this.Equals((VekObj)obj);
            else
                return false;
        }

        /// <summary>
        /// Metóda porovná, či sa this zhoduje s <paramref name="other"/>
        /// </summary>
        /// <param name="other">porovnávaný objekt</param>
        /// <returns>true ak this = <see cref="other"/></returns>
        public bool Equals(VekObj other)
        {
            if (other == null)
                return false; //this určite nie je null

            return
                (this.PocetRokov == other.PocetRokov &&
                this.PocetMesiacov == other.PocetMesiacov &&
                this.PocetDni == other.PocetDni);
        }

        public override int GetHashCode()
        {
            return
                this.PocetRokov.GetHashCode() ^
                this.PocetMesiacov.GetHashCode() ^
                this.pocetDni.GetHashCode();
        }

        /// <summary>
        /// Vek na roky, mesiace a dni slovom.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return
                string.Format(
                    @"{0}, {1}, {2}",
                    this.PocetRokovText,
                    this.PocetMesiacovText,
                    this.PocetDniText);
        }

        #endregion Methods
    }
}